module lucksacks

go 1.16

require (
	github.com/aws/aws-sdk-go v1.40.3
	github.com/aws/aws-sdk-go-v2 v1.17.3
	github.com/aws/aws-sdk-go-v2/config v1.18.7
	github.com/aws/aws-sdk-go-v2/service/dynamodb v1.17.9
	github.com/bcicen/go-units v1.0.2
	github.com/getsentry/sentry-go v0.16.0
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/joho/godotenv v1.3.0
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.9.0
	github.com/slack-go/slack v0.9.1
)
